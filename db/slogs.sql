/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.22-MariaDB : Database - buku_saku
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`buku_saku` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `buku_saku`;

/*Table structure for table `slogs` */

DROP TABLE IF EXISTS `slogs`;

CREATE TABLE `slogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `activity` text DEFAULT NULL,
  `ipaddr` text DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `device` text DEFAULT NULL,
  `platform` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `slogs` */

insert  into `slogs`(`id`,`user`,`activity`,`ipaddr`,`browser`,`device`,`platform`,`created_at`,`updated_at`) values 
(1,'wlp','wlp Login','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:53:20','2022-07-26 06:53:20'),
(2,'sales','sales Login','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:53:56','2022-07-26 06:53:56'),
(3,'sales','sales Membuka menu utama buku saku','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:55:10','2022-07-26 06:55:10'),
(4,'sales','sales Membuka menu utama buku saku','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:57:47','2022-07-26 06:57:47'),
(5,'sales','sales Membuka kategori Selling Point','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:57:59','2022-07-26 06:57:59'),
(6,'sales','sales Membuka kategori Selling Point','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 06:59:50','2022-07-26 06:59:50'),
(7,'sales','sales Membuka list materi dari sub kategori 10 Selling Point Toyota Raize','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 07:00:09','2022-07-26 07:00:09'),
(8,'sales','sales Membaca materi Design Grill Trapezoidal.','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 07:00:57','2022-07-26 07:00:57'),
(9,'sales','sales Membuka menu utama buku saku','127.0.0.1','Chrome 103.0.0.0','WebKit','Windows 10.0','2022-07-26 07:01:11','2022-07-26 07:01:11');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
