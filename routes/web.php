<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MateriController;
use App\Http\Controllers\AdmController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Login PAGE
Route::get('/auth', [LoginController::class, 'loginview'])->name('admlanding');
Route::post('/authprocess', [LoginController::class, 'authprocess']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::group(['middleware'=>['revalidate','auth'],],function(){

    // Menu Utama
    Route::get('/', [HomeController::class, 'index'])->middleware("auth");
    Route::get('/about', [HomeController::class, 'about']);

    // Sub Kategori
    Route::get('/kategori/{ktg_name}', [MateriController::class, 'showkategori']);

    // Detail Kategori
    Route::get('/kategori/sub/{sub_name}', [MateriController::class, 'showmateri']);
    Route::get('/kategori/sub/show/{id_detail}', [MateriController::class, 'showByID']);


    // Kategori
    Route::get('/admin', [AdmController::class, 'showview'])->middleware("auth");
    Route::post('/create/kategori', [AdmController::class, 'create']);
    Route::post('/delete/kategori', [AdmController::class, 'deletektg']);
    Route::post('/update/kategori', [AdmController::class, 'updatektg']);

    // Sub
    Route::get('/sub', [AdmController::class, 'subview'])->middleware("auth");
    Route::get('/sub/input', [AdmController::class, 'showinput']);
    Route::post('/create/sub', [AdmController::class, 'create_kategori']);
    Route::post('/delete/subkategori', [AdmController::class, 'deletesub']);
    Route::get('/update/get/{id_subkategori}', [AdmController::class, 'shwoedit']);
    Route::post('/update/sub', [AdmController::class, 'updatesub']);

    // Materi
    Route::get('/materi', [AdmController::class, 'materiview'])->middleware("auth");
    Route::get('/materi/input', [AdmController::class, 'showinputmateri']);
    Route::get('/materi/getsub', [AdmController::class, 'getsub']);
    Route::post('/create/materi', [AdmController::class, 'savemateri']);
    Route::post('/update/materi', [AdmController::class, 'updatemateri']);
    Route::get('/materi/edit/{id_materi}', [AdmController::class, 'getMateriByID']);

    // Notifikasi
    Route::get('/ceknotif', [AdmController::class, 'notification']);
    Route::get('/setread/{id_notif}', [AdmController::class, 'readnotif']);
    
    // Users
    Route::get('/users', [UserController::class, 'showview']);
    Route::post('/create/users', [UserController::class, 'create']);
    Route::post('/update/users', [UserController::class, 'edit']);
    Route::post('/delete/users', [UserController::class, 'deleteuser']);


});