@extends("layout.admin.header")
@section("contentadm")
<div class="content-wrapper">
    <style>
    .note-editable {
        height: 250px !important;
    }
    </style>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Update Materi</h5>

                            <div class="card-tools">
                                <!-- <a href="/materi/input" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Row
                                </a> -->
                            </div>
                        </div>

                        <form action="/update/materi" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <input type="text" hidden name="id_dtl" id="id_dtl" value="{{$dt['id']}}">
                                            <input type="text" hidden name="id_sub" id="id_sub" value="{{$dt['id_sub']}}">
                                            <label for="kategori" class="form-label">Pilih Kategori</label>
                                            <select type="text" class="form-control" id="chskategori" name="kategori" onchange="setktg()">
                                                <option value="" selected>-- Pilih Kategori --</option>
                                                @foreach($optktg as $kopt => $vopt)
                                                <option value="{{$vopt['id']}}" <?php echo $dt["idktg"] == $vopt["id"] ? "selected" : "" ?>>{{$vopt["nama_kategori"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="nama_subktg" class="form-label">Pilih Kategori</label>
                                            <input type="text" class="form-control frmupdktg" value="{{$dt['subname']}}" disabled>
                                            <div id="showoptsub">

                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="title" class="form-label">Judul Materi</label>
                                            <input type="text" class="form-control" id="title" name="title" value="{{$dt['title']}}">
                                        </div>
                                        <div class="mb-3 input_materi">
                                            <label for="exampleFormControlTextarea1" class="form-label">Example
                                                textarea</label>
                                            <textarea id="summernote" name="description">
                                                <?= base64_decode($dt["description"]) ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="btn-group col-12" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-danger btn-sm btn-block m-0"
                                        data-dismiss="modal" onclick="cancel()">Close</button>
                                    <button type="submit" class="btn btn-primary btn-sm btn-block m-0">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="/adm-assets/dist/js/pages/form.js"></script>
    <script>
    function cancel() {
        window.location.href = "/materi";
    }
    </script>
</div>
@endsection