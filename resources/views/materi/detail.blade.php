@extends("layout.admin.header")
@section("contentadm")
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Materi</h5>

                            <div class="card-tools">
                                <a href="/materi/input" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Materi
                                </a>
                            </div>
                        </div>



                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="overflow-x:auto;">
                                        <table class="table table-hover table-bordered" id="example1">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">Kategori</th>
                                                    <th class="text-center">Sub Kategori</th>
                                                    <th class="text-center">Title</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($getdetail as $key => $value)
                                                <tr>
                                                    <td class="text-center">{{$key+1}}</td>
                                                    <td>{{ $value['nama_kategori'] }}</td>
                                                    <td>{{ $value['subname'] }}</td>
                                                    <td>{{ $value['title'] }}</td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-info mb-2" href="/materi/edit/{{$value['id']}}"><i class="fas fa-edit"></i></a>
                                                        <button class="btn btn-sm btn-danger" onclick="delKtg('<?= $value['id'] ?>')"><i class="fas fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection