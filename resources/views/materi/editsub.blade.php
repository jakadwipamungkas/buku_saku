@extends("layout.admin.header")
@section("contentadm")
<div class="content-wrapper">
    <style>
        .note-editable {
            height: 250px !important;
        }
    </style>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Input Sub Kategori</h5>

                            <div class="card-tools">
                                <!-- <a href="/materi/input" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Row
                                </a> -->
                            </div>
                        </div>

                        <form action="/update/sub" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="kategori" class="form-label">Pilih Kategori</label>
                                            <select type="text" class="form-control" id="kategori" name="kategori">
                                                <option value="" selected>-- Pilih Kategori --</option>
                                                @foreach($optktg as $kopt => $vopt)
                                                <option value="{{$vopt['id']}}" <?php echo $find['id_kategori'] == $vopt['id'] ? 'selected' : '' ?> >{{$vopt["nama_kategori"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <img src="/app-assets/img/{{$find['images']}}" alt="" width="200px">
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8">
                                                    <label for="fileimg" class="form-label">Pilih Gambar <small>(optional)</small></label>
                                                    <input type="file" class="form-control" id="fileimg" name="fileimg" style="padding: 3px;">
                                                    <input type="text" class="form-control" hidden id="id_subktg" name="id_subktg" value="{{$find['id']}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="nama_subktg" class="form-label">Nama Sub Kategori</label>
                                            <input type="text" class="form-control" id="nama_subktg" name="nama_subktg" value="{{$find['subname']}}">
                                        </div>
                                        <div class="mb-3 input_materi d-none">
                                            <label for="exampleFormControlTextarea1" class="form-label">Example
                                                textarea</label>
                                            <textarea id="summernote">
                                                Place <em>some</em> <u>text</u> <strong>here</strong>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="btn-group col-12" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-danger btn-sm btn-block m-0" data-dismiss="modal" onclick="cancel()">Close</button>
                                    <button type="submit" class="btn btn-primary btn-sm btn-block m-0">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function cancel() {
        window.location.href = "/sub";
    }
</script>
@endsection