@extends("layout.admin.header")
@section("contentadm")
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Sub Kategori</h5>

                            <div class="card-tools">
                                <a href="/sub/input" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Sub
                                </a>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="overflow-x:auto;">
                                        <table class="table table-hover table-bordered" id="example1">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">Images</th>
                                                    <th class="text-center">Kategori</th>
                                                    <th class="text-center">Sub Kategori</th>
                                                    <th class="text-center">Dibuat Oleh</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($getsub as $key => $value)
                                                <tr>
                                                    <td class="text-center">{{$key+1}}</td>
                                                    <td class="text-center">
                                                        <img src="/app-assets/img/{{$value['images']}}" alt="" class="img-fluid" style="width: 70px; height: 50px;">
                                                    </td>
                                                    <td>{{ $value['nama_kategori'] }}</td>
                                                    <td>{{ $value['subname'] }}</td>
                                                    <td class="text-center text-uppercase">
                                                        {{ $value['created_by'] }}</td>
                                                    <td class="text-center">
                                                        <a href="/update/get/{{$value['id']}}" class="btn btn-sm btn-info mb-2"><i class="fas fa-edit"></i>&nbsp;Edit</a>
                                                        <button class="btn btn-sm btn-danger" onclick="delKtg('<?= $value['id'] ?>', '<?= $value['subname'] ?>')"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Modal Delete Kategori -->
<div class="modal fade" id="mdlDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle text-danger mt-3 mb-3" style="font-size: 40px;" aria-hidden="true"></i><br>
                <h6 class="modal-title text-center">Delete Sub Kategori <span class="ktgname"></span></h6>
                <form action="/delete/subkategori" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="mb-3">
                        <input type="text" hidden class="form-control" name="idktg" id="idktg">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Delete</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
function delKtg(id, value) {
    $htmlktg = value;
    $("#mdlDelete").modal("show");
    $(".ktgname").html($htmlktg);
    $("#idktg").val(id);
}
</script>
@endsection