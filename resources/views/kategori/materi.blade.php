@extends('layout.headers')
<!-- Pre-loader start -->
<!-- <div class="theme-loader">
    <div class="loader-track">
        <div class="preloader-wrapper">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Pre-loader end -->
@section('content')
<div class="container">
    <div class="row p-2">
        <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-md-left align-items-center mb-2"
                    style="border-radius: 0px; border: 0px;">
                    <a href="/">
                        <ion-icon name="arrow-back-circle" class="text-danger" style="font-size: 25px;"></ion-icon>
                    </a>
                    <span class="p-2 text-kategori" style="font-size: 12px;">{{$sub_kategori}}</span>
                </li>
            </ul>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
            <ul class="list-group">
                @foreach ($getmateri as $key => $value)
                    <a href="/kategori/sub/show/{{$value['id']}}" style="text-decoration: none;">
                        <li class="list-group-item d-flex justify-content-between align-items-center mb-2" style="border-radius: 0px; border: 0px;">
                            <ion-icon name="book" style="font-size: 25px;"></ion-icon>
                            <span class="text-materi">{{$value["title"]}}</span>
                            <ion-icon name="arrow-forward-circle" class="text-danger" style="font-size: 25px;"></ion-icon>
                        </li>
                    </a>
                @endforeach
            </ul>
        </div>
    </div> -->
    @foreach ($getmateri as $key => $value)
    <a href="/kategori/sub/show/{{$value['id']}}" style="text-decoration: none;">
    <div class="row p-2 mb-2 align-items-start bg-white">
        <div class="col-2">
            <ion-icon name="book" style="font-size: 25px;"></ion-icon>
        </div>
        <div class="col">
            <span class="text-materi">{{$value["title"]}}</span>
        </div>
        <div class="col-1">
            <ion-icon name="arrow-forward-circle" class="text-danger" style="font-size: 25px; float: right;"></ion-icon>
        </div>
    </div>
    </a>
    @endforeach
</div>
@endsection