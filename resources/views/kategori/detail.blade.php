@extends('layout.headers')
<!-- Pre-loader start -->
<!-- <div class="theme-loader">
    <div class="loader-track">
        <div class="preloader-wrapper">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Pre-loader end -->
@section('content')
<div class="container">
    <div class="row p-2">
        <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
            <form action="" method="post">
                @csrf
                <!-- <div class="input-group">
                    <input class="form-control" type="search" placeholder="search" style="border-radius: 0px;"
                        id="example-search-input">
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary bg-white border-start-0 border-bottom-0 border ms-n5"
                            style="border-radius: 0px;" type="button">
                            <ion-icon name="search-outline"></ion-icon>
                        </button>
                    </span>
                </div> -->
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
            <?php if (!empty($getSub->toArray())) { ?>
            <ul class="list-group">
                @foreach ($getSub as $key => $value)
                <a href="/kategori/sub/{{$value['subname']}}" style="text-decoration: none;">
                    <li class="list-group-item d-flex justify-content-between align-items-center mb-2"
                        style="border-radius: 0px; border: 0px;">
                        <img src="/app-assets/img/kategori/{{$value['images']}}" alt="" class="img-fluid"
                            style="width: 8vh; height: 6vh;">
                        <span class="text-materi">{{$value["subname"]}}</span>
                        <ion-icon name="arrow-forward-circle" class="text-danger" style="font-size: 25px;"></ion-icon>
                    </li>
                </a>
                @endforeach
            </ul>
            <?php } else { ?>
                <h6 class="text-center">Materi belum tersedia</h6>
            <?php } ?>
        </div>
    </div>
</div>
@endsection