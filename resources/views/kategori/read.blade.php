@extends('layout.headers')
@section('content')
<div class="container">
    <style>
    .desc-materi img {
        width: 70% !important;
        height: 15vh !important;
    }
    </style>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
            <div class="card card-wlp">
                <div class="card-body">
                    <!-- <div id='slides'>
                        <div id='slide1' class="p-2">
                            <ul class="list-group mb-4">
                                <li class="list-group-item d-flex justify-content-md-left align-items-center mb-2"
                                    style="border-radius: 0px; border: 0px;">
                                    <a href="/">
                                        <ion-icon name="arrow-back-circle" class="text-danger" style="font-size: 25px;">
                                        </ion-icon>
                                    </a>
                                    <span class="p-2 text-kategori"
                                        style="font-size: 15px;">{{$showMateri["title"]}}</span>
                                </li>
                            </ul>
                            <?= base64_decode($showMateri["description"]) ?>
                        </div>
                        <?php foreach ($allMateri as $kall => $vall): ?>
                        <div id='<?= 'slide'.$vall["id"] ?>' class="p-2">
                            <ul class="list-group mb-4">
                                <li class="list-group-item d-flex justify-content-md-left align-items-center mb-2"
                                    style="border-radius: 0px; border: 0px;">
                                    <a href="/">
                                        <ion-icon name="arrow-back-circle" class="text-danger" style="font-size: 25px;">
                                        </ion-icon>
                                    </a>
                                    <span class="p-2 text-kategori"
                                        style="font-size: 15px;"><?= $vall["title"] ?></span>
                                </li>
                            </ul>
                            <?= base64_decode($vall["description"]) ?>
                        </div>
                        <?php endforeach; ?>
                        
                    </div> -->
                    <div id="carouselExampleControlsTouching" class="carousel slide" data-bs-touch="true"
                        data-bs-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <ul class="list-group mb-4">
                                    <li class="list-group-item d-flex justify-content-md-left align-items-center mb-2"
                                        style="border-radius: 0px; border: 0px;">
                                        <a href="/">
                                            <ion-icon name="arrow-back-circle" class="text-danger"
                                                style="font-size: 25px;">
                                            </ion-icon>
                                        </a>
                                        <span class="p-2 text-kategori"
                                            style="font-size: 15px;">{{$showMateri["title"]}}</span>
                                    </li>
                                </ul>
                                <?= base64_decode($showMateri["description"]) ?>
                            </div>
                            <?php foreach ($allMateri as $kall => $vall): ?>
                            <div class="carousel-item">
                                <ul class="list-group mb-4">
                                    <li class="list-group-item d-flex justify-content-md-left align-items-center mb-2"
                                        style="border-radius: 0px; border: 0px;">
                                        <a href="/">
                                            <ion-icon name="arrow-back-circle" class="text-danger"
                                                style="font-size: 25px;">
                                            </ion-icon>
                                        </a>
                                        <span class="p-2 text-kategori"
                                            style="font-size: 15px;"><?= $vall["title"] ?></span>
                                    </li>
                                </ul>
                                <?= base64_decode($vall["description"]) ?>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-between mt-2">
        <div class="col-4">
            <button type="button" id="prev" class="prev-slide btn btn-danger btn-sm"
                style="float: left; font-size: 12px;" data-bs-target="#carouselExampleControlsTouching"
                data-bs-slide="prev">Back</button>
        </div>
        <div class="col-4 pull-right">
            <button type="button" id="next" class="next-slide btn btn-danger btn-sm"
                style="float: right; font-size: 12px;" data-bs-target="#carouselExampleControlsTouching"
                data-bs-slide="next">Next</button>
        </div>
    </div>
</div>
@endsection