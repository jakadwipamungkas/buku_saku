@extends('layout.admin.header')
@section('contentadm')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">List Kategori</h5>

                            <div class="card-tools">
                                <button type="button" data-toggle="modal" data-target="#mdlAdd" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Kategori
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="overflow-x:auto;">
                                        <table class="table table-hover table-bordered" id="example1">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">Images</th>
                                                    <th class="text-center">Nama Kategori</th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center">Dibuat Oleh</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($getdata as $key => $value)
                                                <tr>
                                                    <td class="text-center">{{$key+1}}</td>
                                                    <td class="text-center">
                                                        <img src="/app-assets/img/kategori/{{$value['images']}}" alt=""
                                                            class="img-fluid" style="width: 50px; height: 50px;">
                                                    </td>
                                                    <td>{{ $value['nama_kategori'] }}</td>
                                                    @if ($value["status"] == 'active')
                                                        <td class="text-center"><span class="badge badge-success">{{ $value['status'] }}</span></td>
                                                    @endif
                                                    @if ($value["status"] == 'inctive')
                                                        <td class="text-center"><span class="badge badge-danger">{{ $value['status'] }}</span></td>
                                                    @endif
                                                    <td class="text-center text-uppercase">
                                                        {{ $value['created_by'] }}</td>
                                                    <td class="text-center">
                                                        <button class="btn btn-sm btn-info mb-2" onclick="updateKtg('<?= base64_encode(json_encode($value)) ?>')"><i
                                                                class="fas fa-edit"></i>&nbsp;Edit</button>
                                                        <button class="btn btn-sm btn-danger" onclick="delKtg('<?= $value['id'] ?>', '<?= $value['nama_kategori'] ?>')"><i
                                                                class="fas fa-trash"></i>&nbsp;Delete</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<!-- Modal Add Kategori -->
<div class="modal fade" id="mdlAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/create/kategori" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="fileimage" class="form-label">Choose Image</label>
                        <input type="file" class="form-control" name="flimagektg">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama_kategori" id="nama_kategori">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Pilih Status</label>
                        <select type="text" class="form-control" name="status" id="status">
                            <option value="active">Active</option>
                            <option value="inactive">Inctive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Edit Kategori -->
<div class="modal fade" id="mdlUpdate" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/update/kategori" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-4 col-sm-4 showimg">
                            
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <label for="fileimage" class="form-label">Change Image</label>
                            <input type="file" class="form-control" name="upd_flimagektg">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Nama Kategori</label>
                        <input type="text" hidden class="form-control" name="upd_id_kategori" id="upd_id_kategori">
                        <input type="text" hidden class="form-control" name="upd_images_name" id="upd_images_name">
                        <input type="text" class="form-control" name="upd_nama_kategori" id="upd_nama_kategori">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Pilih Status</label>
                        <select type="text" class="form-control" name="upd_status" id="upd_status">
                            <option value="active">Active</option>
                            <option value="inactive">Inctive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Delete Kategori -->
<div class="modal fade" id="mdlDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle text-danger mt-3 mb-3" style="font-size: 40px;" aria-hidden="true"></i><br>
                <h6 class="modal-title text-center">Delete Kategori <span class="ktgname"></span></h6>
                <form action="/delete/kategori" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="mb-3">
                        <input type="text" hidden class="form-control" name="idktg" id="idktg">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Delete</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function updateKtg(value) {
        var dt = JSON.parse(atob(value));
        $("#mdlUpdate").modal("show");
        $("#upd_id_kategori").val(dt.id);
        $("#upd_nama_kategori").val(dt.nama_kategori);
        $("#upd_images_name").val(dt.images);
        $("#upd_status").val(dt.status);
        $(".showimg").html("<img src='/app-assets/img/kategori/"+dt.images+"' alt='' srcset='' style='width: 70px; height: 70px;'>");
    }
    function delKtg(id, value) {
        $htmlktg = value;
        $("#mdlDelete").modal("show");
        $(".ktgname").html($htmlktg);
        $("#idktg").val(id);
    }
</script>
@endsection