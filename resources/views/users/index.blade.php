@extends("layout.admin.header");
@section('contentadm')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">List Users</h5>

                            <div class="card-tools">
                                <button type="button" data-toggle="modal" data-target="#mdlAdd" class="btn btn-tool">
                                    <i class="fas fa-plus"></i> Add Users
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="overflow-x:auto;">
                                        <table class="table table-hover table-bordered" id="example1">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Username</th>
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center">Role</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($getdata as $key => $value)
                                                <tr>
                                                    <td class="text-center">{{$key+1}}</td>
                                                    <td>{{$value["name"]}}</td>
                                                    <td>{{$value["username"]}}</td>
                                                    <td>{{$value["email"]}}</td>
                                                    <td>{{$value["role_name"]}}</td>
                                                    <td class="text-center">
                                                        <button class="btn btn-sm btn-info" onclick="editUser('<?= base64_encode(json_encode($value)) ?>')"><i class="fas fa-edit"></i></button>
                                                        <button class="btn btn-sm btn-danger" onclick="delUser('<?= $value['id'] ?>', '<?= $value['name'] ?>')"><i class="fas fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal Add -->
<div class="modal fade" id="mdlAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Users</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/create/users" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Nama Lengkap</label>
                        <input type="text" class="form-control" name="name" id="name" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Username</label>
                        <input type="text" class="form-control" name="username" id="username" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Password</label>
                        <input type="password" class="form-control" name="password" id="password" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Email</label>
                        <input type="text" class="form-control" name="email" id="email" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Pilih Role</label>
                        <select name="role_id" id="role_id" class="form-control">
                            <option value="" selected>-- Pilih Role --</option>
                            @foreach($listrole as $kr => $vr)
                                <option value="{{$vr['id']}}">{{$vr["role_name"]}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="mdlEdit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Users</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/update/users" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Nama Lengkap</label>
                        <input type="text" hidden class="form-control" name="id_user" id="id_user" required>
                        <input type="text" class="form-control" name="upd_name" id="upd_name" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Username</label>
                        <input type="text" class="form-control" name="upd_username" id="upd_username" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Password</label>
                        <input type="password" class="form-control" name="upd_password" id="upd_password" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Email</label>
                        <input type="text" class="form-control" name="upd_email" id="upd_email" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Pilih Role</label>
                        <select name="upd_role_id" id="upd_role_id" class="form-control">
                            <option value="" selected>-- Pilih Role --</option>
                            @foreach($listrole as $kr => $vr)
                                <option value="{{$vr['id']}}">{{$vr["role_name"]}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Kategori -->
<div class="modal fade" id="mdlDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle text-danger mt-3 mb-3" style="font-size: 40px;" aria-hidden="true"></i><br>
                <h6 class="modal-title text-center">Delete User <span class="nameuser"></span> ?</h6>
                <form action="/delete/users" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="mb-3">
                        <input type="text" hidden class="form-control" name="idusr" id="idusr">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Delete</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="/adm-assets/dist/js/pages/users.js"></script>
@endsection