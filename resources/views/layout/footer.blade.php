<footer class="fixed-bottom">
    <p class="text-footer text-center">Wijaya Toyota</p>
    <p class="text-footer text-center">&copy; <?= date("Y") ?> Jaka Dwi Pamungkas </p>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('/sw.js') }}"></script>
    <script src="{{ asset('/app-assets/js/styles.js') }}"></script>
    <script src="/adm-assets/dist/js/preloaderwlp.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js" integrity="sha512-YYiD5ZhmJ0GCdJvx6Xe6HzHqHvMpJEPomXwPbsgcpMFPW+mQEeVBU6l9n+2Y+naq+CLbujk91vHyN18q6/RSYw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
        });
    }
    </script>

    <script>
    function script() {
        setTimeout(function() {
            document.getElementById('elementToMove').style.left =
                '1000px'; // new left position is 1000px in this example
        }, 2000); // 2000 = 2 seconds after page load
    }
    
    </script>
</footer>

</html>