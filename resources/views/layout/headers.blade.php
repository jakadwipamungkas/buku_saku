<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BUKU SAKU | Wijaya Toyota</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <!-- ICON IONIC -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="{{ asset('/app-assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/app-assets/css/preload.css') }}">
    <link rel="shortcut icon" href="/app-assets/img/logo-mini.png" type="image/x-icon" />

    <!-- PWA -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <link rel="stylesheet" href="/adm-assets/dist/css/preloaderwlp.css">

</head>

<body onload="script();">
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__wobble" src="/app-assets/img/logo-mini.png" alt="AdminLTELogo" height="50" width="60">
    </div>
    <!-- Start TOP -->
    <div class="wrapper m-0 p-0">
        <nav class="navbar" style="background: #212529;">
            <div class="container-fluid">
                <a class="navbar-brand text-white text-title" href="/">BUKU SAKU</a>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button class="navbar-toggler" style="border: none;" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                            <ion-icon name="notifications" class="text-white"></ion-icon>
                            <span class="badge badge-warning badge-count" style="font-size: 10px; position: absolute; color: white; font-weight: bold;"></span>
                        </button>
                        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                            <div class="offcanvas-header">
                                <h4 class="offcanvas-title" id="offcanvasNavbarLabel">Notifikasi</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body shownotif">
                            
                            </div>
                        </div>
                        <a class="navbar-toggler" href="/logout" type="button" style="border: none;">
                            <ion-icon name="power" class="text-danger"></ion-icon>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <div id="elementToMove">
            @yield('content')
        </div>

    </div>
</body>

@extends('layout.footer')