<aside class="control-sidebar control-sidebar-dark">

</aside>

<footer class="main-footer">
    &copy; WLP
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.22
    </div>
</footer>
</div>

<script src="/adm-assets/plugins/jquery/jquery.min.js"></script>
<script src="/adm-assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/adm-assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/adm-assets/dist/js/adminlte.js"></script>
<script src="/adm-assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="/adm-assets/dist/js/pages/dashboard2.js"></script>
<!-- DataTables  & Plugins -->
<script src="/adm-assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/adm-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/adm-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/adm-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/adm-assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/adm-assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="/adm-assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="/adm-assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="/adm-assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- Summernote -->
<script src="/adm-assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
$("#example1").DataTable({});
$('#summernote').summernote({
    fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20'],
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['link', 'picture', 'video']],
        ['fontname', ['fontname']],
    ]
})
</script>
</body>

</html>