<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS Buku Saku | Wijaya Toyota</title>


    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <link rel="stylesheet" href="/adm-assets/plugins/fontawesome-free/css/all.min.css">

    <link rel="stylesheet" href="/adm-assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

    <link rel="stylesheet" href="/adm-assets/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="/adm-assets/css/styleadm.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="/adm-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/adm-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/adm-assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    <!-- summernote -->
    <link rel="stylesheet" href="/adm-assets/plugins/summernote/summernote-bs4.min.css">
</head>

<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">


        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__wobble" src="/app-assets/img/logo-mini.png" alt="AdminLTELogo" height="50"
                width="60">
        </div>

        <nav class="main-header navbar navbar-expand navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search"
                                    aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout" role="button">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <a href="index3.html" class="brand-link">
                <img src="/app-assets/img/logo-mini.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
                <span class="brand-text font-weight-light">CMS Buku Saku</span>
            </a>

            <div class="sidebar">

                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="/adm-assets/dist/img/man.png" class="img-circle elevation-2"
                            alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block text-uppercase">{{ auth()->user()->username }}</a>
                    </div>
                </div>

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <a href="/admin" class="nav-link">
                            <li class="nav-item nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Homepage
                                </p>
                            </li>
                        </a>
                        <a href="/sub" class="nav-link">
                            <li class="nav-item nav-link">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    Sub Kategori
                                </p>
                            </li>
                        </a>
                        <a href="/materi" class="nav-link">
                            <li class="nav-item nav-link">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    Materi
                                </p>
                            </li>
                        </a>
                        <a href="/users" class="nav-link">
                            <li class="nav-item nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Users
                                </p>
                            </li>
                        </a>
                    </ul>
                </nav>
            </div>
        </aside>
        @yield('contentadm')
@extends("layout.admin.footer")