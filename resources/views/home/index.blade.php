@extends('layout.headers')

<!-- Pre-loader start -->
<!-- <div class="theme-loader">
    <div class="loader-track">
        <div class="preloader-wrapper">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- Pre-loader end -->
@section('content')
<div class="navbar mt-0 p-5 banner">

</div>
<div class="row justify-content-center m-0" style="top: -40px; z-index: 2; position: relative; width: 100%;">
    @foreach ($getdata as $key => $value)
    <div class="col-6 mb-3 text-center">
        <a href="<?php echo $value['alias'] == 'about' ? '/about' : '/kategori'. "/" . $value['alias'] ;?>" style="text-decoration: none;">
            <div class="card p-3">
                <div class="card-body text-center">
                    <img src="/app-assets/img/kategori/{{$value['images']}}" class="img-fluid" style="width: 83px; height: 72px;"
                        alt=""><br>
                    <div class="wrap-text mt-2" style="width: 80%; margin: 0 auto;">
                        <span class="text-kategori">{{$value["nama_kategori"]}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>
@endsection