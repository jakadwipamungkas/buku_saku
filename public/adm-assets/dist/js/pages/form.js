function setktg() {
    var ktg = $("#chskategori").val();
    $.ajax({
        url: "/materi/getsub",
        type: "get",
        data: {
            id:ktg
        },
        success: function (response) {
            $(".frmupdktg").hide();
            var dt = JSON.parse(response).data;
            var html;
            html = '<select type="text" class="form-control" id="subkategori" name="subkategori">';
            $.each(dt, function (k, v) {
                html += '<option value="'+v['id']+'">'+v['subname']+'</option>';
            });
            html += '</select>';

            $("#showoptsub").html(html);
        },
        error: function(response) {
            console.log(textStatus, errorThrown);
        }
    });
}