
// TOUCH SWIPING
$(function () {
    $(".carousel").carousel({
        interval: false,
        pause: true,
        touch: true,
    });
    // enable prev/next navigation
    $(".carousel .carousel-control-prev").on("click", function () {
        $(".carousel").carousel("prev");
    });

    $(".carousel .carousel-control-next").on("click", function () {
        $(".carousel").carousel("next");
    });

    $(".carousel .carousel-inner").swipe({
        swipeLeft: function (event, direction, distance, duration, fingerCount) {
            this.parent().carousel("next");
        },
        swipeRight: function () {
            this.parent().carousel("prev");
        },
        threshold: 0,
        tap: function (event, target) {
            window.location = $(this).find(".carousel-item.active a").attr("href");
        },
        excludedElements: "label, button, input, select, textarea, .noSwipe",
    });
});

slideW = $("#slides").width();
current = 0;
$(document).on("click", "#prev", function (e) {
    if (current > 0 && current <= $("#slides").children().length - 1) {
        current--;
    }

    e.preventDefault();
    $("#slides").animate(
        {
            scrollLeft: slideW * current - 0,
        },
        600
    );
});


$(document).on("click", "#next", function (e) {
    if (current < $("#slides").children().length - 1) current++;
    e.preventDefault();
    $("#slides").animate(
        {
            scrollLeft: slideW * current + 0,
        },
        600
    );
});

function ceknotif() {
    $.ajax({
        url: "/ceknotif",
        type: "get",
        success: function (response) {
            var dt = JSON.parse(response);
            $(".badge-count").html(dt.new);
            var html = "";
            $.each(dt.data, function (key, val) {
                html += '<ol class="list-group">';
                html +=
                    '<a style="text-decoration: none;" href="/setread/' +
                    val["id"] +
                    '"><li class="list-group-item list-group-item-action d-flex justify-content-between align-items-start">';
                html += '    <div class="ms-2 me-auto">';
                html +=
                    '        <div class="fw-bold">' + val["title"] + "</div>";
                html +=
                    '        <span style="font-size: 12px;">' +
                    val["message"] +
                    "</span>";
                html += "    </div>";
                if (val["status"] == "send")
                    html +=
                        '    <span class="badge bg-info rounded-pill">new</span>';
                html += "</li></a>";
                html += "</ol>";
            });
            $(".shownotif").html(html);
        },
        error: function (response) {
            console.log(textStatus, errorThrown);
        },
    });
}

setInterval(ceknotif, 1000);
