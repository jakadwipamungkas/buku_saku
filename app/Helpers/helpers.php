<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Carbon\Carbon;

use Jenssegers\Agent\Agent;

use App\Models\Slog as LogDB;

function insertLog($act)
{
    $agent  = new Agent;
    $log    = new LogDB; 
    
    $log->user      = Auth()->user()->username;
    $log->activity  = Auth()->user()->username . " " . $act;
    $log->ipaddr    = request()->ip();
    $log->browser   = $agent->browser() . " " . $agent->version($agent->browser());
    $log->device    = $agent->device();
    $log->platform  = $agent->platform() . " " . $agent->version($agent->platform());

    $log->save();
}

?>