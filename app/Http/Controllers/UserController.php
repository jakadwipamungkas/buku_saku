<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as UserDB;
use App\Models\Role as RoleDB;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    public function showview(Request $request)
    {
        $getdata    = UserDB::selectraw("users.*, roles.role_name")->join("roles", "roles.id", "=", "users.role_id")->get()->toArray();
        $listrole   = RoleDB::get()->toArray();
        return view("users.index", compact("getdata", "listrole"));
    }

    public function create(Request $request)
    {
        $user = new UserDB;

        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->password     = bcrypt($request->password);
        $user->temp         = Crypt::encryptString($request->password);
        $user->email        = $request->email;
        $user->role_id      = $request->role_id;

        $user->save();
        return redirect("/users");
    }

    public function edit(Request $request)
    {
        $user = UserDB::find($request->id_user);
        $user->name         = $request->upd_name;
        $user->username     = $request->upd_username;
        $user->password     = bcrypt($request->upd_password);
        $user->temp         = $request->upd_password;
        $user->email        = $request->upd_email;
        $user->role_id      = $request->upd_role_id;
        $user->updated_at   = date("Y-m-d H:i:s");
        
        $user->save();
        return redirect("/users");
    }

    public function deleteuser(Request $request)
    {
        $users = UserDB::find($request->idusr)->delete();
        return redirect('/users');
    }
}
