<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori as KategoriDB;
use App\Models\Subkategori as SubktgDB;
use App\Models\Notif as NotifDB;
use App\Models\Detail_subkategori as DkDB; // Detail Kategori
use App\Models\User as UserDB;

class AdmController extends Controller
{
    public function showview(Request $request)
    {
        $getdata = KategoriDB::get()->toArray();
        return view("admin.index", compact("getdata"));
    }

    public function create(Request $request)
    {
        $path = public_path() . "\app-assets\img\kategori";
        if (!empty($_FILES)) {
            $temp = explode(".", $_FILES["flimagektg"]["name"]);
            $filename = "kategori_".strtolower($request->nama_kategori). '_' . date("YmdHis") . '.' . end($temp);
            $kategori = new KategoriDB;
           
            $kategori->nama_kategori = $request->nama_kategori;
            $kategori->alias         = str_replace(" ", "_", $request->nama_kategori);
            $kategori->images        = $filename;
            $kategori->status        = $request->status;
            $kategori->created_by    = auth()->user()->username;
            $kategori->created_at    = date("Y-m-d H:i:s");
            
            if (!file_exists($path) && !is_dir($path)) {
                mkdir($path, 0777, true);
            }
            if(move_uploaded_file($_FILES['flimagektg']['tmp_name'], $path . '/' . $filename)){
                $kategori->save();
            }
        }
        return redirect('/admin');
    }


    public function updatektg(Request $request)
    {
        $path = public_path() . "\app-assets\img\kategori";

        if($_FILES["upd_flimagektg"]["name"] != "") {
            $temp = explode(".", $_FILES["upd_flimagektg"]["name"]);
            $filename = "kategori_".strtolower($request->upd_nama_kategori). '_' . date("YmdHis") . '.' . end($temp);
            $kategori = KategoriDB::find($request->upd_id_kategori);
           
            $kategori->nama_kategori = $request->upd_nama_kategori;
            $kategori->alias         = str_replace(" ", "_", $request->upd_nama_kategori);
            $kategori->images        = $filename;
            $kategori->status        = $request->upd_status;
            $kategori->updated_by    = auth()->user()->username;
            $kategori->updated_at    = date("Y-m-d H:i:s");

            if (!file_exists($path) && !is_dir($path)) {
                mkdir($path, 0777, true);
            }
            if(move_uploaded_file($_FILES['upd_flimagektg']['tmp_name'], $path . '/' . $filename)){
                $kategori->save();
            }
            
            return redirect("/admin");
        } else {
            $kategori = KategoriDB::find($request->upd_id_kategori);
            $kategori->nama_kategori = $request->upd_nama_kategori;
            $kategori->alias         = str_replace(" ", "_", $request->upd_nama_kategori);
            $kategori->images        = $request->upd_images_name;
            $kategori->status        = $request->upd_status;
            $kategori->updated_by    = auth()->user()->username;
            $kategori->updated_at    = date("Y-m-d H:i:s");
            $kategori->save();
            return redirect("/admin");
        }
    }

    public function deletektg(Request $request)
    {
        $kategori = KategoriDB::find($request->idktg)->delete();
        return redirect('/admin');
    }


    // SUb Kategori & Materi

    public function subview(Request $request)
    {
        $getsub = SubktgDB::selectraw('subkategoris.*, kategoris.nama_kategori')->join('kategoris', 'subkategoris.id_kategori', '=', 'kategoris.id')->get()->toArray();
        
        return view("materi.sub", compact("getsub"));
    }

    public function showinput(Request $request)
    {
        $optktg = KategoriDB::selectraw("id, nama_kategori")->get()->toArray();
        return view('materi.input', compact('optktg'));
    }

    public function create_kategori(Request $request)
    {
        $path = public_path() . "\app-assets\img";
        if (!empty($_FILES)) {
            $temp = explode(".", $_FILES["fileimg"]["name"]);
            $filename = "subkategori_".strtolower($request->nama_subktg). '_' . date("YmdHis") . '.' . end($temp);

            $subktg = new SubktgDB;
            $subktg->id_kategori    = $request->kategori;
            $subktg->subname        = $request->nama_subktg;
            $subktg->images         = $filename;
            $subktg->is_child       = 1;
            $subktg->created_by     = auth()->user()->username;
            $subktg->created_at     = date("Y-m-d H:i:s");
            
            if (!file_exists($path) && !is_dir($path)) {
                mkdir($path, 0777, true);
            }
            if(move_uploaded_file($_FILES['fileimg']['tmp_name'], $path . '/' . $filename)){
                $subktg->save();
            }
        }
        return redirect('/admin');
    }

    public function deletesub(Request $request)
    {
        // print_r("<pre>");
        // print_r($request->idktg);
        // exit();
        $kategori   = SubktgDB::find($request->idktg)->delete();
        $delDtl     = DkDB::where("id_sub", $request->idktg)->delete();
        return redirect('/admin');
    }

    // Materi
    public function materiview(Request $request)
    {
        $getdetail = KategoriDB::selectraw("detail_subkategoris.*, subkategoris.subname, kategoris.nama_kategori")
                        ->leftjoin('subkategoris', 'kategoris.id', '=', 'subkategoris.id_kategori') 
                        ->leftjoin('detail_subkategoris', 'subkategoris.id', '=', 'detail_subkategoris.id_sub') 
                        ->get()
                        ->toArray();

        return view("materi.detail", compact("getdetail"));
    }

    public function showinputmateri(Request $request)
    {
        $optktg = KategoriDB::selectraw("id, nama_kategori")->get()->toArray();
        return view("materi.form", compact("optktg"));
    }

    public function getsub(Request $request)
    {
        $getsub = SubktgDB::where("id_kategori", $request->id)->get()->toArray();
        
        $data = [
            "data" => $getsub
        ];

        return json_encode($data);
    }

    public function savemateri(Request $request)
    {
        $materi = new DkDB;
        $materi->id_sub         = $request->subkategori;
        $materi->title          = $request->title;
        $materi->description    = base64_encode($request->description);
        $materi->created_at     = date("Y-m-d H:i:s");
        $materi->created_by     = auth()->user()->username;

        $getdt = SubktgDB::where("id", $request->subkategori)->first()->toArray();
        $getktg = KategoriDB::where("id", $getdt["id_kategori"])->first()->toArray();

        if ($materi->save()) {
            $notif = new NotifDB;
            $notif->id_sub  = $request->subkategori;
            $notif->title   = "Materi Baru";
            $notif->message = "Materi Kategori " . $getktg["nama_kategori"] . " telah rilis";
            $notif->status  = "send";
            $notif->created_by = auth()->user()->username;
            $notif->created_at = date("Y-m-d H:i:s");
            $notif->save();
        }
        return redirect('/materi');
    }

    // Update Materi
    public function getMateriByID(Request $request)
    {
        $dt     = KategoriDB::selectraw("detail_subkategoris.*, subkategoris.subname, kategoris.id as idktg")
                        ->leftjoin('subkategoris', 'kategoris.id', '=', 'subkategoris.id_kategori') 
                        ->leftjoin('detail_subkategoris', 'subkategoris.id', '=', 'detail_subkategoris.id_sub')
                        ->where("detail_subkategoris.id", $request->id_materi)->first()->toArray();

        $optktg = KategoriDB::selectraw("id, nama_kategori")->get()->toArray();

        return view("materi.editform", compact("optktg", "dt"));
    }

    public function updatemateri(Request $request)
    {
        $materi = DkDB::find($request->id_dtl);
        $materi->id_sub         = $request->subkategori != "" ? $request->subkategori : $request->id_sub;
        $materi->title          = $request->title;
        $materi->description    = base64_encode($request->description);
        $materi->updated_at     = date("Y-m-d H:i:s");
        $materi->updated_by     = auth()->user()->username;

        $materi->save();
        return redirect('/materi');
    }

    // Notifikasi
    public function notification(Request $request)
    {
        $getnotif = NotifDB::where("status", "send")->get()->toArray();
        $getNew = NotifDB::where("status", "send")->get()->toArray();
        $data = [
            "data"  => $getnotif,
            "new"   => count($getNew)
        ];
        return json_encode($data);
    }

    public function readnotif(Request $request)
    {
        $notif = NotifDB::find($request->id_notif);
        $getsub =  SubktgDB::find($notif->id_sub);
        // print_r("<pre>");
        // print_r($getsub);
        // exit();
        $notif->status = "read";
        $notif->save();
        return redirect("/kategori/sub/".$getsub["subname"]);
    }

    public function shwoedit(Request $request)
    {
        // print_r("<pre>");
        $optktg = KategoriDB::selectraw("id, nama_kategori")->get()->toArray();
        $find = SubktgDB::find($request->id_subkategori);
        // print_r($find->toArray());
        // exit();
        return view("materi.editsub", compact("optktg", "find"));
    }

    public function updatesub(Request $request)
    {
        $path = public_path() . "\app-assets\img";
        if($_FILES["fileimg"]["name"] != "") {
            $temp = explode(".", $_FILES["fileimg"]["name"]);
            $filename = "subkategori_".strtolower($request->nama_subktg). '_' . date("YmdHis") . '.' . end($temp);

            $subktg = SubktgDB::find($request->id_subktg);
            $subktg->id_kategori    = $request->kategori;
            $subktg->subname        = $request->nama_subktg;
            $subktg->images         = $filename;
            $subktg->is_child       = 1;
            $subktg->updated_by     = auth()->user()->username;
            $subktg->updated_at     = date("Y-m-d H:i:s");
            
            if (!file_exists($path) && !is_dir($path)) {
                mkdir($path, 0777, true);
            }
            if(move_uploaded_file($_FILES['fileimg']['tmp_name'], $path . '/' . $filename)){
                $subktg->save();
            }
            return redirect("/sub");
        } else {
            $subktg = SubktgDB::find($request->id_subktg);
            $subktg->id_kategori    = $request->kategori;
            $subktg->subname        = $request->nama_subktg;
            $subktg->is_child       = 1;
            $subktg->updated_by     = auth()->user()->username;
            $subktg->updated_at     = date("Y-m-d H:i:s");
            $subktg->save();
            return redirect("/sub");
        }
    }

    public function editsub(Request $request)
    {
        print_r("<pre>");
        print_r($request->all());
        exit();
    }
    public function getedit(Request $request)
    {
        print_r("<pre>");
        print_r($request->id_sub);
        exit();
        return view("materi.editsub");
    }
}
