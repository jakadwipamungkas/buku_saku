<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Materi as MateriDB;
use App\Models\Kategori as KategoriDB;
use App\Models\Subkategori as SubktgDB;
use App\Models\Detail_subkategori as DkDB; // Detail Kategori

class MateriController extends Controller
{
    public function showkategori(Request $request)
    {
        $kategori = KategoriDB::where("alias", $request->ktg_name)->first()->toArray();
        insertLog("Membuka kategori " . $kategori["nama_kategori"]);
        $getSub = SubktgDB::selectraw("images, subname, is_child")->where("id_kategori", $kategori["id"])->get();
        if (!empty($getSub)) {
            return view("kategori.detail", compact("getSub"));
        } else {
            return view("kategori.detail", compact("getSub"));
        }
    }

    public function showmateri(Request $request)
    {
        $sub_kategori       = $request->sub_name;
        $subkategori        = SubktgDB::where("subname", $request->sub_name)->first();
        insertLog("Membuka list materi dari sub kategori " . $request->sub_name);
        $getmateri          = DkDB::selectraw("id, id_sub, title, description")->where("id_sub", $subkategori->id)->get()->toArray();

        if (!empty($getmateri)) {
            return view("kategori.materi", compact("getmateri", "sub_kategori"));
        } else {
            return view("kategori.materi", compact("getmateri", "sub_kategori"));
        }
    }

    public function showByID(Request $request)
    {
        $id_detail      = $request->id_detail;
        $showMateri     = DkDB::selectraw("id, id_sub, title, description")->where("id", $id_detail)->first()->toArray();
        $allMateri      = DkDB::selectraw("id, id_sub, title, description")->where("id_sub", $showMateri["id_sub"])->whereNotIn("id", [$id_detail])->get()->toArray();

        insertLog("Membaca materi " . $showMateri["title"]);
        
        return view("kategori.read", compact("showMateri", "allMateri"));
    }
}
