<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User as UserDB;

class LoginController extends Controller
{
    public function loginview(Request $request)
    {
        return view("login.index");
    }

    public function authprocess(Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);
            
        $checkuser = UserDB::where("username", $request->username)->first();
 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            
            insertLog("Login");
            
            if ($checkuser->role_id == 1) {
                return redirect()->intended('admin');
            } elseif ($checkuser->role_id == 2) {
                return redirect()->intended('/');
            }
        }
 
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
 
        request()->session()->invalidate();
    
        request()->session()->regenerateToken();
    
        return redirect('/auth');
    }
}
