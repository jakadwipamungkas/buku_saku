<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori as KategoriDB;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        insertLog("Membuka menu utama buku saku");
        $getdata = KategoriDB::get()->toArray();
        return view("home.index", compact('getdata'));
    }

    public function about(Request $request)
    {
        return view ("home.about");
    }
}
